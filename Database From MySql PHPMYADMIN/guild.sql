-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 17 Jan 2021 pada 11.06
-- Versi server: 10.4.13-MariaDB
-- Versi PHP: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `guild`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `goods`
--

CREATE TABLE `goods` (
  `good_kode` int(11) NOT NULL,
  `name` varchar(40) NOT NULL,
  `rank` varchar(11) NOT NULL,
  `price` int(90) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `herb`
--

CREATE TABLE `herb` (
  `herb_id` int(11) NOT NULL,
  `name` text NOT NULL,
  `rank` text NOT NULL,
  `value` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `hunter`
--

CREATE TABLE `hunter` (
  `id_hunter` int(11) NOT NULL,
  `nama_hunter` varchar(40) NOT NULL,
  `rank` varchar(11) NOT NULL,
  `power` int(100) NOT NULL,
  `money` int(90) NOT NULL,
  `id_party` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `hunter`
--

INSERT INTO `hunter` (`id_hunter`, `nama_hunter`, `rank`, `power`, `money`, `id_party`) VALUES
(1924, 'Momonga', '', 74, 10000, 1),
(2941, 'Rimuru', '', 86, 1000, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `monster`
--

CREATE TABLE `monster` (
  `monster_id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `rank` text NOT NULL,
  `price` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `party`
--

CREATE TABLE `party` (
  `id_party` int(11) NOT NULL,
  `nama_party` varchar(40) NOT NULL,
  `rank_party` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `party`
--

INSERT INTO `party` (`id_party`, `nama_party`, `rank_party`) VALUES
(1, 'ainz ooal gown', ''),
(2, 'jura tempest federation', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `quest`
--

CREATE TABLE `quest` (
  `id_quest` int(11) NOT NULL,
  `nama_quest` varchar(200) NOT NULL,
  `rank_quest` varchar(11) NOT NULL,
  `tipe_quest` varchar(20) NOT NULL,
  `maxhunter` int(11) NOT NULL,
  `minpower` int(11) NOT NULL,
  `reward` int(11) NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id_user` varchar(11) NOT NULL,
  `nama_user` varchar(40) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `umur` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `id_hunter` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id_user`, `nama_user`, `gender`, `umur`, `username`, `password`, `id_hunter`) VALUES
('20001', 'Leka Budi Putra', 'Laki-laki', 19, 'LBP', '1234', 2941),
('20002', 'Arie Hidayattullah', 'Laki-laki', 20, 'zeelynn', 'klee', 1924);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `goods`
--
ALTER TABLE `goods`
  ADD PRIMARY KEY (`good_kode`);

--
-- Indeks untuk tabel `herb`
--
ALTER TABLE `herb`
  ADD PRIMARY KEY (`herb_id`);

--
-- Indeks untuk tabel `hunter`
--
ALTER TABLE `hunter`
  ADD PRIMARY KEY (`id_hunter`),
  ADD KEY `id_party` (`id_party`);

--
-- Indeks untuk tabel `monster`
--
ALTER TABLE `monster`
  ADD PRIMARY KEY (`monster_id`);

--
-- Indeks untuk tabel `party`
--
ALTER TABLE `party`
  ADD PRIMARY KEY (`id_party`);

--
-- Indeks untuk tabel `quest`
--
ALTER TABLE `quest`
  ADD PRIMARY KEY (`id_quest`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`),
  ADD KEY `id_hunter` (`id_hunter`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `goods`
--
ALTER TABLE `goods`
  MODIFY `good_kode` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `monster`
--
ALTER TABLE `monster`
  MODIFY `monster_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `party`
--
ALTER TABLE `party`
  MODIFY `id_party` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `quest`
--
ALTER TABLE `quest`
  MODIFY `id_quest` int(11) NOT NULL AUTO_INCREMENT;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `hunter`
--
ALTER TABLE `hunter`
  ADD CONSTRAINT `hunter_ibfk_1` FOREIGN KEY (`id_party`) REFERENCES `party` (`id_party`);

--
-- Ketidakleluasaan untuk tabel `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_2` FOREIGN KEY (`id_hunter`) REFERENCES `hunter` (`id_hunter`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
