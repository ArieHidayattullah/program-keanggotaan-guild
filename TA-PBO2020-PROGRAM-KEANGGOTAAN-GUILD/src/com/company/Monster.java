package com.company;

public class Monsters extends GameObject {
    private int power;

    public Monsters(String nama, int power){
        super(nama);
        this.power = power;
    }

    @Override
    public String getRank(){
        if(power > 300){
            return "Rank S";
        }else if(power <= 300 || power > 200){
            return "Rank A (Legendary)";
        }else if(power <= 200 || power > 80){
            return "Rank B (Epic)";
        }else if(power <= 80 || power > 30){
            return "Rank C (Rare)";
        }else{
            return "underrated";
        }
    }
}