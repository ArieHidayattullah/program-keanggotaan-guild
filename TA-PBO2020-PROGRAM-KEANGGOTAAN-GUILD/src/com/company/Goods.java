package com.company;

public class Goods extends GameObject {
    private int price;

    public Goods(String nama, int price){
        super(nama);
        this.price = price;
    }

    @Override
    public String getRank() {
        if(price > 100){
            return "Rank S";
        }else if(price <= 100 || price > 50){
            return "Rank A";
        }else if(price <= 50 || price > 10){
            return "Rank B";
        }else if(price <= 10 || price > 1){
            return "Rank C";
        }else{
            return "underrated";
        }
    }
}