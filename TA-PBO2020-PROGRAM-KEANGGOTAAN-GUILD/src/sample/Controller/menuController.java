package sample.Controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.event.ActionEvent;
import sample.utils.ConnectorDB;

import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import java.net.URL;
import java.sql.Statement;

public class menuController {
    @FXML
    private ImageView logOutButton;
    @FXML
    private ImageView listQuestButton;
    @FXML
    private ImageView listMemberButton;
    @FXML
    private ImageView listHerbButton;
    @FXML
    private ImageView listgoodButton;
    @FXML
    private ImageView listMonsterButton;
    @FXML
    private Parent listQuestpanel;
    @FXML
    private Parent listmemberPanel;
    @FXML
    private Parent listHerbPanel;
    @FXML
    private Parent listGoodPanel;
    @FXML
    private Parent listMonsterPanel;

    public void initialize(URL url, ResourceBundle resourceBundle){
        //logout
        File logoutFile = new File("image/logout.png");
        Image logoutImage = new Image(logoutFile.toURI().toString());
        logOutButton.setImage(logoutImage);
        //list quest
        File listQuestFile = new File("image/listofquests.png");
        Image listQuestImage = new Image(listQuestFile.toURI().toString());
        listQuestButton.setImage(listQuestImage);
        //list Member
        File listMemberFile = new File("image/listofmembers.png");
        Image listMemberImage = new Image(listMemberFile.toURI().toString());
        listMemberButton.setImage(listMemberImage);
        //list Herb
        File listHerbFile = new File("image/listofherbs.png");
        Image listHerbImage = new Image(listHerbFile.toURI().toString());
        listHerbButton.setImage(listHerbImage);
        //list good
        File listgoodFile = new File("image/listofgoods.png");
        Image listgoodImage = new Image(listgoodFile.toURI().toString());
        listgoodButton.setImage(listgoodImage);
        //list monster
        File listMonsterFile = new File("image/listofmonster.png");
        Image listMonsterImage = new Image(listMonsterFile.toURI().toString());
        listMonsterButton.setImage(listMonsterImage);
    }

    public void logOutButtonOnAction(ActionEvent event){
        Stage stage = (Stage) logOutButton.getScene().getWindow();
        stage.close();
    }

    public void listQuestButtonOnAction(ActionEvent event){
        listQuestpanel.disableProperty();
    }

    public void listMemberButtonOnAction(ActionEvent event){
        listmemberPanel.disableProperty();
    }
    public void listHerbButtonOnAction(ActionEvent event){
        listHerbPanel.disableProperty();
    }
    public void listgoodButtonOnAction(ActionEvent event){
        listGoodPanel.disableProperty();
    }
    public void listMonsterButtonOnAction(ActionEvent event){
        listMonsterPanel.disableProperty();
    }

}
