package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import javafx.stage.StageStyle;
import sample.item.Guild;
import sample.item.GuildMaster;
import sample.utils.DBConnector;
public class Main extends Application {

    Guild guild = new Guild("Petualang");
    GuildMaster guildMaster = new GuildMaster("Sebasian","laki-laki",
            45,"guildterhebat","Sebas");

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("view/login.fxml"));
        primaryStage.initStyle(StageStyle.UNDECORATED);
        primaryStage.setScene(new Scene(root, 520, 400));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
