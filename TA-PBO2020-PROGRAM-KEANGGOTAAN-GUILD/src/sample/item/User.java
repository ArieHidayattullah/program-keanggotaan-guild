package sample.item;

public class User {
    private String name;
    private String gender;
    private int age;
    private String username;
    private String password;

    public User() {
    }
    public User(String username, String password){
        this.username = username;
        this.password = password;
    }
    public User(String name, String gender, int age, String password, String username) {
        this.name = name;
        this.gender = gender;
        this.age = age;
        this.password = password;
        this.username = username;
    }

    //getter
    public String getName() {
        return name;
    }
    public String getGender() {
        return gender;
    }
    public int getAge() {
        return age;
    }
    public String getUsername() {
        return username;
    }
    public String getPassword() {
        return password;
    }

    //setter
    public void setName(String name) {
        this.name = name;
    }
    public void setGender(String gender) {
        this.gender = gender;
    }
    public void setAge(int age) {
        this.age = age;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public void setPassword(String password) {
        this.password = password;
    }
}
