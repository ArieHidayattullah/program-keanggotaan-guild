package sample.item;

public class Herb extends gameObject {
    private int value;

    public Herb(String nama, int value){
        super(nama);
        this.value = value;
    }

    @Override
    public String getRank() {
        if(value <= 100 || value > 90){
            return "Rank S (Mythical)";
        }else if(value <= 90 || value > 80){
            return "Rank A (Legendary)";
        }else if(value <= 80 || value > 70){
            return "Rank B (Epic)";
        }else if(value <= 70 || value > 50){
            return "Rank C (Rare)";
        }else{
            return "underrated";
        }
    }
}
